import { combineReducers } from 'redux';
import { Todo } from './interfaces';
import { PayloadedAction } from '../interface';
import {
    ADD_TODO,
    TOGGLE_TODO,
    DELETE_TODO,
    EDIT_TODO,
    CHANGE_TODO,
    CHANGE_FILTER,
    TOGGLE_EDITING,
    ALL,
    TODO_SEARCH,
    CLEAR_SEARCH,
    SELECT_PAGE,
    LOAD_TODOS
} from './constants';
import { List, Map } from 'immutable';

const todos = (state: List<Map<string, any>> = List<Todo>([]), action: PayloadedAction): List<Map<string, any>> => {
    switch (action.type) {
        case ADD_TODO:
            return state.push(Map({
                id: action.payload.id,
                description: action.payload.description,
                done: false
            }));
        case DELETE_TODO:
            return state.delete(state.findIndex(todo => todo.get('id') == action.payload.id));
        case EDIT_TODO:
            return state.update(
                state.findIndex(todo => todo.get('id') == action.payload.id),
                todo => todo.set('description', action.payload.value)
            );
        case TOGGLE_TODO:
            return state.update(
                state.findIndex(todo => todo.get('id') == action.payload.id),
                todo => todo.set('done', action.payload.done)
            );
        case LOAD_TODOS:
            return state = List<Todo>(action.payload.map(
                (todo: Todo) => {
                    return Map(todo)
                }
            ));
        default:
            return state;
    }
};

const todoInput = (state = '', action: PayloadedAction): string => {
    switch (action.type) {
        case CHANGE_TODO:
            return action.payload;
        case ADD_TODO:
            return '';
        default:
            return state;
    }
};

const filter = (state: string = ALL, action: PayloadedAction): string => {
    switch (action.type) {
        case CHANGE_FILTER:
            return action.payload;
        default:
            return state;
    }
};

const editableTodo = (state: number | null = null, action: PayloadedAction): number | null => {
    switch (action.type) {
        case TOGGLE_EDITING:
            return action.payload;
        case EDIT_TODO:
            return state = null;
        default:
            return state;
    }
};

const searchInput = (state = '', action: PayloadedAction): string => {
    switch (action.type) {
        case TODO_SEARCH:
            return action.payload;
        case CLEAR_SEARCH:
            return state = '';
        default:
            return state
    }
};

const currentPage = (state = 0, action: PayloadedAction): number => {
    switch (action.type) {
        case SELECT_PAGE:
            return action.payload;
        case TODO_SEARCH:
            return state = 0;
        default:
            return state;
    }
};

export const todoApp = combineReducers({
    todos,
    todoInput,
    filter,
    editableTodo,
    searchInput,
    currentPage
});