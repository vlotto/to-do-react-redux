import * as React from 'react';
import { connect } from 'react-redux';
import { MainPageDomain } from '../../interfaces';
import { Store } from '../../../interface';
import { Menu } from '../../components/Menu/index';
import { UserMenu } from '../../components/UserMenu/index';
import { StatusMessage } from '../../components/StatusMessage/index';
import {
    logOut,
    checkLocalStorage,
    deleteMessage
} from '../../actions';

const mapStateToProps = (state: Store): MainPageDomain => {
    const { mainPage } = state;
    return { ...mainPage };
};

interface MainPageDispatchProps {
    logOut: () => void;
    deleteMessage: () => void;
    checkLocalStorage: () => void;
}

const mapDispatchToProps = (dispatch: any): MainPageDispatchProps => ({
    logOut: () => {
        dispatch(logOut())
    },
    checkLocalStorage: () => {
        dispatch(checkLocalStorage())
    },
    deleteMessage: () => {
        dispatch(deleteMessage())
    }
});

type MainPageProps = MainPageDomain & MainPageDispatchProps;

class MainPage extends React.Component<MainPageProps, void> {
    public componentWillMount() {
        this.props.checkLocalStorage();
    }

    public render() {
        return (
            <div>
                {!localStorage.getItem('token') ? <Menu /> : <UserMenu logOut={this.props.logOut}/>}
                <main>
                    <StatusMessage deleteMessage={this.props.deleteMessage} statusMessage={this.props.statusMessage}/>
                    {this.props.children}
                </main>
            </div>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MainPage);