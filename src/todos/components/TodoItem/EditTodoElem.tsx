import * as React from 'react';
import { EditTodoElementProps } from './index';
const styles = require('./styles');
const cx = require("classnames");

export const EditTodoElem = (props: EditTodoElementProps) => (
    <input
        className={cx(styles.edit, {[styles.show] : props.editableTodo == props.todo.id})}
        defaultValue={props.todo.description}
        onKeyDown={(e) => {
            switch (e.key) {
                case 'Enter':
                    return props.endTodoEdit(props.todo.id, (e.target as HTMLInputElement).value);
                case 'Escape':
                    return props.endTodoEdit(props.todo.id, (e.target as HTMLInputElement).value = props.todo.description);
            }
        }}
        onBlur={(e) => props.endTodoEdit(props.todo.id, (e.target as HTMLInputElement).value)}
        ref={(input) => {
            if (props.todo.id == props.editableTodo && input != null) {
                input.focus();
            }
        }}
    />
);