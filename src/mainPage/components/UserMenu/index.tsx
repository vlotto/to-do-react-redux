import * as React from 'react';
const styles = require('./styles');

interface UserMenuProps {
    logOut: () => void;
}

export class UserMenu extends React.Component<UserMenuProps, void> {
    public render() {
        const { logOut } = this.props;
        return (
            <div>
                <nav className={styles.navBar}>
                    <a href="#" onClick={logOut}>log out</a>
                </nav>
            </div>
        )
    }
}