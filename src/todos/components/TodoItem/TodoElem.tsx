import * as React from 'react';
import { TodoElementProps } from './index';
const styles = require('./styles');
const cx = require("classnames");

export const TodoElem = (props: TodoElementProps) => (
    <div>
        <div className={cx(styles.complete, {[styles.completed] : props.todo.done})}
             onClick={() => props.getToggleTodo(props.todo.id)}>
        </div>

        <span className={styles.description}
              onDoubleClick={() => props.toggleEditing(props.todo.id)}> {props.todo.description} </span>
        <div className={styles.delete} onClick={() => props.deleteRequest(props.todo.id)}></div>
    </div>
);