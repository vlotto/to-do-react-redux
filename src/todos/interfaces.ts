import { List, Map } from 'immutable';

export interface Todo extends Map<string, any> {
    id: number;
    description: string;
    done: boolean;
}

export interface TodoAppDomain {
    editableTodo: number | null;
    todoInput: string;
    filter: string;
    searchInput: string;
    currentPage: number;
    todos: List<Todo>;
}