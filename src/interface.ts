import { Action } from "redux";
import { TodoAppDomain } from './todos/interfaces';
import { AuthorizationDomain } from './authorization/interfaces';
import { MainPageDomain } from './mainPage/interfaces';

export interface Store {
    todoApp: TodoAppDomain;
    authorization: AuthorizationDomain;
    mainPage: MainPageDomain;
}

export interface PayloadedAction extends Action {
    payload?: any;
}