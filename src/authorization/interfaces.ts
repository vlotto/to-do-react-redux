export interface AuthorizationDomain {
    usernameInput: string;
    passwordInput: string;
    confirmPasswordInput: string;
}

export interface authorizationProps {
    changeUserName: (value: string) => void;
    changePassword: (value: string) => void;
    usernameInput: string;
    passwordInput: string;
}