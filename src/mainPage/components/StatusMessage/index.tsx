import * as React from 'react';
const styles = require('./styles');

interface StatusMessageProps {
    statusMessage: string;
    deleteMessage: () => void;
}

export class StatusMessage extends React.Component<StatusMessageProps, void> {
    public render() {
        const { statusMessage, deleteMessage } = this.props;
        const userName = localStorage.getItem('userName');
        return (
            <div>
                {statusMessage &&
                <div className={styles.over}>
                    <div className={styles.message}><p>{statusMessage}</p>
                        <span className={styles.close} onClick={deleteMessage}>&times;</span>
                    </div>
                </div>
                }
            </div>
        )
    }
}