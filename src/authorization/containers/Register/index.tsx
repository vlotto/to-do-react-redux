import * as React from 'react';
import { connect } from 'react-redux';
import { AuthorizationDomain } from '../../interfaces';
import { Store } from '../../../interface';
import {
    changeUserName,
    changePassword,
    checkConfirmPassword,
    changeConfirmPassword,
    clearInput
} from '../../actions';
import { Registration } from '../../components/Registration/index';

const mapStateToProps = (state: Store): AuthorizationDomain => {
    const { authorization } = state;
    return { ...authorization };
};

interface RegistrationDispatchProps {
    changeUserName: (value: string) => void;
    changeConfirmPassword: (value: string) => void;
    changePassword: (value: string) => void;
    checkConfirmPassword: () => void;
    clearInput: () => void;
}
const mapDispatchToProps = (dispatch: any): RegistrationDispatchProps => ({
    changeUserName: (value: string) => {
        dispatch(changeUserName(value))
    },
    changePassword: (value: string) => {
        dispatch(changePassword(value))
    },
    changeConfirmPassword: (value: string) => {
        dispatch(changeConfirmPassword(value))
    },
    checkConfirmPassword: () => {
        dispatch(checkConfirmPassword())
    },
    clearInput: () => {
        dispatch(clearInput())
    }
});

type RegisterProps = AuthorizationDomain & RegistrationDispatchProps;

class Register extends React.Component<RegisterProps, void> {
    public componentWillMount() {
        this.props.clearInput();
    }
    public render() {
        return (
            <div className="forms">
                <Registration {...this.props}/>
            </div>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Register);