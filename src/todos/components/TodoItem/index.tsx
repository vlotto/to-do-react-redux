import * as React from 'react';
import { EditTodoElem } from './EditTodoElem';
import { TodoElem } from './TodoElem';
import { Todo } from '../../interfaces';
const styles = require('./styles');
const cx = require("classnames");

interface BaseTodoElementProps {
    todo: Todo;
}

export interface EditTodoElementProps extends BaseTodoElementProps {
    editableTodo: number;
    endTodoEdit: (id: number, value: string) => void;
}

export interface TodoElementProps extends BaseTodoElementProps {
    getToggleTodo: (id: number) => void;
    deleteRequest: (id: number) => void;
    toggleEditing: (id: number) => void;
}

type TodoItemProps = EditTodoElementProps & TodoElementProps;

export const TodoItem = (props: TodoItemProps) => (
    <li className={cx(styles.todoItem, {[styles.itemComplete] : props.todo.done})}>
        <TodoElem toggleEditing={props.toggleEditing} todo={props.todo}
                  getToggleTodo={props.getToggleTodo}
                  deleteRequest={props.deleteRequest}/>
        {props.editableTodo == props.todo.id &&
        <EditTodoElem todo={props.todo} editableTodo={props.editableTodo}
                      endTodoEdit={props.endTodoEdit}/>}
    </li>
);