import * as React from 'react';
const styles = require('./styles');

interface AddTodoProps {
    todoInput: string;
    submitTodo: () => void;
    changeTodo: (value: string) => void;
}

export class AddTodo extends React.Component<AddTodoProps, void> {
    public render() {
        const { todoInput, changeTodo, submitTodo } = this.props;

        return (
            <form className={styles.form}
                  onSubmit={(e) => {
                      e.preventDefault();
                      submitTodo();
                  }}>
                <input className={styles.input} placeholder="new todo" type="text"
                       value={todoInput}
                       onChange={(e) => changeTodo((e.target as HTMLInputElement).value)}/>
            </form>
        )
    }
}