import { push } from 'redux-router';
import { PayloadedAction, Store } from '../interface';
import { REQUEST_STATUS, DELETE_MESSAGE, ADD_CURRENT_TOKEN } from './constants';

export const statusMessage = (message: any): PayloadedAction => ({
    type: REQUEST_STATUS,
    payload: message
});

export const deleteMessage = (): PayloadedAction => ({
    type: DELETE_MESSAGE
});

const addCurrentToken = (token: string): PayloadedAction => ({
    type: ADD_CURRENT_TOKEN,
    payload: token
});

export const redirect = (path: string) =>
    (dispatch: Function): void => {
        dispatch(push(path));
    };

export const checkLocalStorage = () =>
    (dispatch: Function, getState: () => Store): void => {
        const { mainPage: { currentToken } } = getState();
        if (localStorage.getItem('token')) {
            dispatch(redirect('/todo/'));
        } else {
            dispatch(redirect('/reg/'));
        }
    };

export const logOut = () =>
    (dispatch: Function): void => {
        localStorage.clear();
        dispatch(redirect('/login/'));
    };

export const addToken = (token: string) =>
    (dispatch: Function): void => {
        localStorage.setItem('token', token);
        dispatch(addCurrentToken(token));
        dispatch(checkLocalStorage());
    };


