import { createSelector } from 'reselect'
import { List } from 'Immutable';
import { Todo } from './interfaces';
import { ALL, COMPLETED, ACTIVE } from './constants';
import { Store } from '../interface';

export const getVisibleTodos = createSelector(
    (state: Store) => state.todoApp.filter,
    (state: Store) => state.todoApp.todos,
    (state: Store) => state.todoApp.searchInput,
    (filter: string, todos: List<Todo>, searchInput: string): any => {
        switch (filter) {
            case ALL:
                return todos.filter(todo => todo.get('description').match(searchInput));
            case COMPLETED:
                return todos.filter(todo => todo.get('done') && todo.get('description').match(searchInput));
            case ACTIVE:
                return todos.filter(todo => !todo.get('done') && todo.get('description').match(searchInput));
        }
    }
);