import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
const ReduxThunk = require('redux-thunk').default;
import { createStore, applyMiddleware, compose } from 'redux';

import { reduxReactRouter, ReduxRouter, routerStateReducer } from 'redux-router';
import { Route } from 'react-router'
import { createHistory } from 'history';

import { todoApp } from './todos/reducers';
import { authorization } from './authorization/reducers';
import { mainPage } from './mainPage/reducers';
import TodoApp from './todos/containers/TodoApp/index';
import SignIn from './authorization/containers/SignIn/index';
import Register from './authorization/containers/Register/index';
import MainPage from './mainPage/containers/MainPage/index';
import { combineReducers } from 'redux';
const styles = require('./styles');

const AllReducers = combineReducers({
    todoApp,
    authorization,
    mainPage,
    router: routerStateReducer
});

const store = createStore(AllReducers, compose(
    applyMiddleware(ReduxThunk),
    reduxReactRouter({ createHistory }),
    (window as any)['devToolsExtension'] ? (window as any).devToolsExtension() : (f: any) => f
));

ReactDOM.render(
    <Provider store={store}>
        <ReduxRouter>
            <Route path="/" component={MainPage}>
                <Route path="/login" component={SignIn}/>
                <Route path="/reg" component={Register}/>
                <Route path="/todo" component={TodoApp}/>
                <Route path="*" component={Register}/>
            </Route>
        </ReduxRouter>
    </Provider>,
    document.getElementById('root')
);