import { Store, PayloadedAction } from '../interface';
import {
    CHANGE_USER_NAME,
    CHANGE_PASSWORD,
    CHANGE_CONFIRM_PASSWORD,
    CLEAR_INPUT
} from './constants';
import { redirect } from '../mainPage/actions'
import { statusMessage } from '../mainPage/actions'
import { addToken } from '../mainPage/actions'

declare const fetch: any;

export const clearInput = (): PayloadedAction => ({
    type: CLEAR_INPUT,
});

export const changeUserName = (userNameInput: string): PayloadedAction => ({
    type: CHANGE_USER_NAME,
    payload: userNameInput
});

export const changePassword = (passwordInput: string): PayloadedAction => ({
    type: CHANGE_PASSWORD,
    payload: passwordInput
});

export const changeConfirmPassword = (confirmPasswordInput: string): PayloadedAction => ({
    type: CHANGE_CONFIRM_PASSWORD,
    payload: confirmPasswordInput
});

export const loginRequest = () =>
    (dispatch: Function, getState: () => Store): Promise<any> => {
        const { authorization: { usernameInput, passwordInput } } = getState();
        return fetch('/api/oauth2/access_token/', {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            body: (`username=${usernameInput}&password=${passwordInput}&grant_type=password&client_id=${usernameInput}`)
        })
            .then((response: any) => {
                    if (response.status !== 200) {
                        return response.json()
                            .then((json: any) => dispatch(statusMessage(JSON.stringify(json.error).replace(/"/gi, ""))));
                    }
                    return response.json()
                        .then((json: any) => dispatch(addToken(JSON.stringify(json.access_token).replace(/"/gi, ""))));
                }
            )
    };

const registrationRequest = () =>
    (dispatch: Function, getState: () => Store): Promise<any> => {
        const { authorization: { usernameInput, passwordInput } } = getState();
        return fetch('/api/register/', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({ username: usernameInput, password: passwordInput })
        })
            .then((response: any) => {
                    if (response.status !== 201) {
                        dispatch(statusMessage('Error registration'));
                        return;
                    }
                    return response.json()
                        .then(dispatch(redirect('/login/')))
                        .then(dispatch(statusMessage('Successful registration')));
                }
            )
    };

export const checkConfirmPassword = () =>
    (dispatch: Function, getState: () => Store): void => {
        const { authorization: { confirmPasswordInput, passwordInput, usernameInput } } = getState();
        if (confirmPasswordInput !== passwordInput) {
            dispatch(statusMessage('Error, confirmation passwords do not match passwords'))
        } else if (usernameInput) {
            dispatch(registrationRequest());
        }
    };
