import * as React from 'react';
import { connect } from 'react-redux';
import {
    submitTodo,
    changeTodo,
    patchTodoComplete,
    changeFilter,
    deleteRequest,
    endTodoEdit,
    toggleEditing,
    todoSearch,
    selectPage,
    clearSearch,
    initialLoadTodos,
} from '../../actions';
import { AddTodo } from '../../components/AddTodo/index';
import { TodoList } from '../../components/TodoList/index';
import { Filter } from '../../components/Filter/index'
import { Search } from '../../components/Search/index'
import { TodoAppDomain } from '../../interfaces';
import { Store } from '../../../interface';
import { getVisibleTodos } from '../../selectors';
const styles = require('./styles');
const logoPng = require('./images/logo-png');
const logoSvg = require('./images/logo');

const mapStateToProps = (state: Store): TodoAppDomain => {
    const { todoApp: { todoInput, editableTodo, searchInput, currentPage, filter } } = state;
    return {
        todoInput,
        editableTodo,
        searchInput,
        currentPage,
        filter,
        todos: getVisibleTodos(state),
    }
};


interface TodoAppDispatchProps {
    changeTodo: (todo: string) => void;
    submitTodo: () => void;
    patchTodoComplete: (id: number) => void;
    changeFilter: (value: string) => void;
    deleteRequest: (id: number) => void;
    endTodoEdit: (id: number, value: string) => void;
    toggleEditing: (id: number) => void;
    todoSearch: (value: string) => void;
    selectPage: (numberPage: number) => void;
    clearSearch: () => void;
    initialLoadTodos: () => void;
}

const mapDispatchToProps = (dispatch: any): TodoAppDispatchProps => ({
    changeTodo: (todo: string) => {
        dispatch(changeTodo(todo))
    },
    submitTodo: () => {
        dispatch(submitTodo())
    },
    patchTodoComplete: (id: number) => {
        dispatch(patchTodoComplete(id))
    },
    changeFilter: (value: string) => {
        dispatch(changeFilter(value))
    },
    deleteRequest: (id: number) => {
        dispatch(deleteRequest(id))
    },
    endTodoEdit: (id: number, value: string) => {
        dispatch(endTodoEdit(id, value))
    },
    toggleEditing: (id: number) => {
        dispatch(toggleEditing(id))
    },
    todoSearch: (value: string) => {
        dispatch(todoSearch(value))
    },
    selectPage: (numberPage: number) => {
        dispatch(selectPage(numberPage))
    },
    clearSearch: () => {
        dispatch(clearSearch())
    },
    initialLoadTodos: () => {
        dispatch(initialLoadTodos())
    }
});

type TodoAppProps = TodoAppDomain & TodoAppDispatchProps;

class TodoApp extends React.Component<TodoAppProps, void> {
    public componentWillMount() {
        this.props.initialLoadTodos();
    }

    public render() {
        return (
            <div>
                <img className={styles.logo} src={logoPng}/>
                <h1>Todos</h1>
                <Search clearSearch={this.props.clearSearch} todoSearch={this.props.todoSearch}
                        searchInput={this.props.searchInput}/>
                <AddTodo submitTodo={this.props.submitTodo} changeTodo={this.props.changeTodo}
                         todoInput={this.props.todoInput}/>
                <TodoList getToggleTodo={this.props.patchTodoComplete}
                          todos={this.props.todos.toJS()}
                          deleteRequest={this.props.deleteRequest}
                          endTodoEdit={this.props.endTodoEdit}
                          editableTodo={this.props.editableTodo}
                          toggleEditing={this.props.toggleEditing}
                          selectPage={this.props.selectPage}
                          currentPage={this.props.currentPage}
                />
                <Filter changeFilter={this.props.changeFilter}/>
                <object className={styles.coop} type="image/svg+xml" data={logoSvg}/>
            </div>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TodoApp);