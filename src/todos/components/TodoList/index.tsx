import { TodoItem } from '../../components/TodoItem/index';
import * as React from 'react';
import { Todo } from '../../interfaces';
const styles = require('./styles');
const cx = require("classnames");

interface TodoListProps {
    currentPage: number;
    editableTodo: number;
    todos: Todo[];
    selectPage: (pageNumber: number) => void;
    getToggleTodo: (id: number) => void;
    deleteRequest: (id: number) => void;
    toggleEditing: (id: number) => void;
    endTodoEdit: (id: number, value?: string) => void;
}

export class TodoList extends React.Component<TodoListProps, void> {
    public render() {
        const { todos, getToggleTodo, deleteRequest, endTodoEdit, toggleEditing, editableTodo, selectPage, currentPage } = this.props;
        let paginationList = [];
        const pageSize = 5;
        const startIndex = pageSize * currentPage;
        const endIndex = startIndex + pageSize;
        const getPage = todos.slice(startIndex, endIndex);
        const amountOfPages = Math.ceil(todos.length / pageSize);

        for (let pageNumber = 0; pageNumber < amountOfPages; pageNumber++) {
            paginationList.push(<span className={cx({[styles.select] : pageNumber == currentPage})}
                                      onClick={() => selectPage(pageNumber)} key={pageNumber}>{pageNumber + 1}</span>);
        }

        return (
            <div>
                <ul className={styles.list}>
                    {getPage == null || getPage.map(todo =>
                        <TodoItem getToggleTodo={getToggleTodo} toggleEditing={toggleEditing}
                                  deleteRequest={deleteRequest}
                                  editableTodo={editableTodo} endTodoEdit={endTodoEdit} key={todo.id}
                                  todo={todo}/>)}
                </ul>
                <div className={styles.paginator}>
                    {paginationList.length > 1 && paginationList}
                </div>
            </div>
        )
    }
}