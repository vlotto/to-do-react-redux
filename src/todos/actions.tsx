import {
    ADD_TODO,
    TOGGLE_TODO,
    DELETE_TODO,
    EDIT_TODO,
    CHANGE_TODO,
    CHANGE_FILTER,
    TOGGLE_EDITING,
    TODO_SEARCH,
    CLEAR_SEARCH,
    SELECT_PAGE,
    LOAD_TODOS
} from './constants';
import { statusMessage } from '../mainPage/actions'
import { Todo } from './interfaces';
import { Store, PayloadedAction } from '../interface';
declare const fetch: any;

//HTTP
const loadTodos = (todos: Todo[]): PayloadedAction => ({
    type: LOAD_TODOS,
    payload: todos
});

export const initialLoadTodos = () =>
    (dispatch: Function): void => {
        if (localStorage.getItem('token')) {
            const token = localStorage.getItem('token');
            return fetch('/api/todos/', {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${token}`
                },
                method: 'GET'
            }).then((response: any) => {
                    if (response.status !== 200) {
                        dispatch(statusMessage(response));
                        return;
                    }
                    return response.json()
                        .then((json: Todo[]) => dispatch(loadTodos(json)));
                }
            )
        }
    };

const addTodo = (todo: Todo): PayloadedAction => ({
    type: ADD_TODO,
    payload: todo
});

const postTodo = (todoInput: string) =>
    (dispatch: Function): void => {
        const token = localStorage.getItem('token');
        return fetch('/api/todos/', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            },
            method: 'POST',
            body: JSON.stringify({ 'description': todoInput })
        }).then((response: any) => {
                if (response.status !== 201) {
                    dispatch(statusMessage('Add error'));
                    return;
                }
                return response.json()
                    .then((json: Todo) => dispatch(addTodo(json)));
            }
        )

    };

export const submitTodo = () =>
    (dispatch: Function, getState: () => Store): void => {
        const { todoApp: { todoInput } } = getState();
        if (todoInput) {
            dispatch(postTodo(todoInput));
        }
    };

const deleteTodo = (id: number): PayloadedAction => ({
    type: DELETE_TODO,
    payload: id
});

export const deleteRequest = (id: number) =>
    (dispatch: Function): void => {
        const token = localStorage.getItem('token');
        return fetch(`/api/todos/${id}/`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `bearer ${token}`
            },
            method: 'DELETE'
        })
            .then((response: any) => {
                    if (response.status !== 204) {
                        dispatch(statusMessage('Sorry, server does not allow delete request'));
                        return;
                    }
                    return dispatch(deleteTodo(id));
                }
            )
    };

function putRequest(id: number, body: any): Promise<any> {
    const token = localStorage.getItem('token');
    return fetch(`/api/todos/${id}`, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        method: 'PUT',
        body: JSON.stringify(body)
    })
}

const toggleTodo = (id: number, done: boolean): PayloadedAction => ({
    type: TOGGLE_TODO,
    payload: { id, done }
});

export const patchTodoComplete = (id: number) =>
    (dispatch: Function, getState: () => Store): Promise<any> => {
        const { todoApp: { todos } } = getState();
        const done = !todos.find((todo: Todo) => todo.get('id') == id).get('done');
        const description = todos.find((todo: Todo) => todo.get('id') == id).get('description');
        return putRequest(id, { 'description': description, 'done': done })
            .then((response: any) => {
                    if (response.status !== 200) {
                        dispatch(statusMessage('error'));
                        console.log(response);
                        return;
                    }
                    return dispatch(toggleTodo(id, done));
                }
            );
    };

const editTodo = (id: number, value: string, done: boolean): PayloadedAction => ({
    type: EDIT_TODO,
    payload: { id, value, done }
});

const patchTodoTask = (id: number, value: string) =>
    (dispatch: Function, getState: () => Store): Promise<any> => {
        const { todoApp: { todos } } = getState();
        let done = todos.find((todo: Todo) => todo.get('id') == id).get('done');
        return putRequest(id, { 'description': value, 'done': done })
            .then((response: any) => {
                    if (response.status !== 200) {
                        alert('Edit error');
                        return;
                    }
                    return dispatch(editTodo(id, value, done));
                }
            );
    };

export const endTodoEdit = (id: number, value: string) =>
    (dispatch: Function): void => {
        if (value) {
            dispatch(patchTodoTask(id, value));
        } else {
            dispatch(deleteRequest(id));
        }
    };

//Local
export const changeTodo = (todoInput: string): PayloadedAction => ({
    type: CHANGE_TODO,
    payload: todoInput
});

export const changeFilter = (value: string): PayloadedAction => ({
    type: CHANGE_FILTER,
    payload: value
});

export const toggleEditing = (id: number): PayloadedAction => ({
    type: TOGGLE_EDITING,
    payload: id
});

export const todoSearch = (value: string): PayloadedAction => ({
    type: TODO_SEARCH,
    payload: value
});

export const selectPage = (numberPage: number): PayloadedAction => ({
    type: SELECT_PAGE,
    payload: numberPage
});

export const clearSearch = (): PayloadedAction => ({
    type: CLEAR_SEARCH
});
