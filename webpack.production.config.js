const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = {
    entry: {
        bundle: './src/index.tsx'
    },
    output: {
        path: __dirname + '/build/',
        filename: 'js/[name].js',
        publicPath: '../build/'
    },
    resolve: {
        extensions: ["", ".ts", ".tsx", ".js", ".jsx", ".scss", ".png", ".svg"]
    },
    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loader: "babel!ts",
                exclude: [/node_modules/],
                include: __dirname + '/src',
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('style', 'css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]!sass')
            },
            {
                test: /.*\.(gif|png|jpe?g|svg)$/i,
                loader: "file?name=img/[ext]/[name].[ext]"
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('css/style.css', {
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            template: './src/index.ejs',
            appMountId: 'root',
            title: 'Todoapp',
            inject: true
        }),
    ]
};
module.exports = config;