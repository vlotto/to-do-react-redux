import * as React from 'react';
import { connect } from 'react-redux';
import { AuthorizationDomain } from '../../interfaces';
import { Store } from '../../../interface';
import {
    changeUserName,
    changePassword,
    loginRequest,
    clearInput
} from '../../actions';
import { Login } from '../../components/Login/index';

const mapStateToProps = (state: Store): AuthorizationDomain => {
    const { authorization } = state;
    return { ...authorization };
};

interface AuthorizationDispatchProps {
    changeUserName: (value: string) => void;
    changePassword: (value: string) => void;
    loginRequest: () => void;
    clearInput: () => void;
}
const mapDispatchToProps = (dispatch: any): AuthorizationDispatchProps => ({
    changeUserName: (value: string) => {
        dispatch(changeUserName(value))
    },
    changePassword: (value: string) => {
        dispatch(changePassword(value))
    },
    loginRequest: () => {
        dispatch(loginRequest())
    },
    clearInput: () => {
        dispatch(clearInput())
    }
});

type AuthorizationProps = AuthorizationDomain & AuthorizationDispatchProps;

class SignIn extends React.Component<AuthorizationProps, void> {
    public componentWillMount() {
        this.props.clearInput();
    }

    public render() {
        return (
            <div className="forms">
                <Login {...this.props}/>
            </div>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SignIn);