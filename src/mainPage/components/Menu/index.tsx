import * as React from 'react';
import { Link } from 'react-router'
const styles = require('./styles');

interface MenuProps {

}

export class Menu extends React.Component<MenuProps, void> {
    public render() {
        return (
            <div>
                <nav className={styles.navBar}>
                    <Link to={`/reg`} activeClassName="active">Register</Link>
                    <Link to={`/login`} activeClassName="active">Authorization</Link>
                </nav>
            </div>
        )
    }
}