import * as React from 'react';
const styles = require('./styles');

interface FilterProps {
    changeFilter: (value: string) => void;
}

export class Filter extends React.Component<FilterProps, void> {
    public render() {
        const {changeFilter} = this.props;

        return (
            <span className={styles.filter}>
               Show
                <select name="filter_select"
                        onChange={(e) => changeFilter((e.target as HTMLSelectElement).value)}>
                    <option value="ALL">All</option>
                    <option value="COMPLETED">Completed</option>
                    <option value="ACTIVE">Active</option>
                </select>
                todos.
             </span>
        )
    }
}