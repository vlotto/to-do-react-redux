import * as React from 'react';
const styles = require('./styles');
const cx = require("classnames");

interface SearchProps {
    searchInput: string;
    clearSearch: () => void;
    todoSearch: (value: string) => void;
}

export class Search extends React.Component<SearchProps, void> {
    public render() {
        const { todoSearch, searchInput, clearSearch } = this.props;
        return (
            <div className={cx(styles.search, {[styles.show]: searchInput})}>
                <input className={styles.input}
                       placeholder="Find Todo"
                       value={searchInput}
                       onChange={(e) => todoSearch((e.target as HTMLInputElement).value)}
                />
                {searchInput && <span onClick={() => clearSearch()} className={styles.clear}>+</span>}
            </div>
        )
    }
}